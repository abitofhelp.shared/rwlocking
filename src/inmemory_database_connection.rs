use std::sync::Arc;

use parking_lot::RwLock;
use crate::database_connection::DatabaseConnection;

use crate::project_entity::ProjectEntity;


// InMemoryDatabase is a mutable "data store".
// RwLock permits multiple, concurrent readers, but only a single, exclusive writer.
pub type InMemoryDatabase = RwLock<Vec<ProjectEntity>>;

// InMemoryDatabaseConnection is a thread-safe, shared access to the inmemory database.
pub type InMemoryDatabaseConnection = Arc<DatabaseConnection<InMemoryDatabase>>;

pub fn new() -> InMemoryDatabaseConnection {
    let mut inmemory_database = RwLock::new(vec![
        ProjectEntity::new(None, "abc".to_string(), 123, "America_Phoenix".to_string()).unwrap(),
        ProjectEntity::new(None, "xyz".to_string(), 321, "America_Chicago".to_string()).unwrap(),
    ]);

    InMemoryDatabaseConnection::from(DatabaseConnection {
        database: inmemory_database,
    })
}
