use crate::error::ApiError;
use crate::inmemory_database_connection::InMemoryDatabaseConnection;
use crate::project::Project;
use crate::project_entity_mapper::ProjectEntityMapper;

mod database_connection;
mod error;
mod inmemory_database_connection;
mod project;
mod project_entity;
mod project_entity_mapper;

fn main() -> std::io::Result<()> {
    let database_connection = inmemory_database_connection::new();

    let project = Project::from("zzz", 010, "UTC");
    create_project(database_connection.clone(), project).map_err(|e| panic!("{e}"))
}

fn create_project(conn: InMemoryDatabaseConnection, project: Project) -> Result<(), ApiError> {

    let project_name = project.name.clone();

    {
        println!(
            "Searching for an existing project named '{}'",
            &project.name
        );

        {
            let read_binding = conn.database.read();
            let exists = read_binding.iter().find(|p| p.name == project_name);
            if exists.is_some() {
                return Err(ApiError::NotFound {
                    message: format!("a project named '{}' already exists", &project_name),
                    err: None,
                });
            }
        }

        {
            println!("A project named '{}' does not exist", &project_name);
            let project_entity = ProjectEntityMapper::to_entity(project);
            println!(
                "Attempting to create() the project named '{}'",
                &project_name
            );
            let mut write_binding = conn.database.write();
            let result = write_binding.push(project_entity);
            println!("Created the project named '{}'", &project_name);
            Ok(result)
        }
    }
}

// IS IT POSSIBLE TO CHAIN THE READ AND WRITE?  BASED ON MY READING AND EXPERIMENTING, IT LOOKS
// LIKE THE FOLLOWING IS NOT FEASIBLE.
//
// conn.database
//     .read().iter()
//     .find(|p| p.name == project.name)
//     .map_or_else(
//         || {
//             println!(
//                 "A project named '{}' does not exist -- proceeding with create()",
//                 &project.name
//             );
//             let project_entity = ProjectEntityMapper::to_entity(project);
//             println!("Attempting to create() the project");
//             conn.database.write().push(project_entity);
//
//             Ok(())
//         },
//         |pe| {
//             Err(ApiError::NotFound {
//                 message: format!("a project named '{}' already exists", pe.name),
//                 err: None,
//             })
//         },
//     )
// }
