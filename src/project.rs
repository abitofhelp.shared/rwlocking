// Represents a list of projects
pub type ProjectList = Vec<Project>;

// Represents a project
#[derive(Clone, Debug, Default, Eq, PartialEq)]
#[non_exhaustive]
pub struct Project {
    pub name: String,
    pub size: usize,
    pub timezone: String,
}

impl Project {
    #[inline]
    pub const fn new(name: String, size: usize, timezone: String) -> Self {
        Self {
            name,
            size,
            timezone,
        }
    }

    #[inline]
    pub fn from(name: &str, size: usize, timezone: &str) -> Self {
        Self::new(String::from(name), size, String::from(timezone))
    }
}
